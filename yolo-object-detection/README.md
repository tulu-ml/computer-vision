---
title: Yolo Object Detection
emoji: 🐨
colorFrom: indigo
colorTo: red
sdk: gradio
sdk_version: 4.1.1
app_file: app.py
pinned: false
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
