from fastapi import FastAPI
from service.api.api import main_router
import onnxruntime as rt

app = FastAPI(project_name="Emotion Detection")
app.include_router(main_router)


# majority of time is spent on loading the model.
# so load the model in main file
providers = ['CPUExecutionProvider']
m_q = rt.InferenceSession("service/efficient_net_quantized.onnx", providers=providers)


@app.get("/")
async def root():
    return {"hello":"world"}